import { ObjectId } from "mongodb";

export interface User {
  _id: ObjectId;
  name: {
    prefix: string | undefined;
    firstName: string;
    lastName: string;
    suffix: string | undefined;
  };
  age: number;
  address: {
    country: string;
    state: string | undefined;
    city: string;
    zip: string;
    coordinates: {
      type: "Point";
      coordinates: [number, number];
    } | undefined;
  } | undefined;
}

export interface WorkBase {
  _id: ObjectId;
  authors: ObjectId[];
  references: ObjectId[];
  submittedAt: Date;
  field: string;

  title: string;
  subtitle: string | undefined;
}

export interface Article extends WorkBase {
  readonly type: 'article';

  journal: string;
  edition: string;
  reviewedBy: ObjectId[];
}

export interface Whitepaper extends WorkBase {
  readonly type: 'white-paper';

  publication: ObjectId | undefined;
}

export interface Book extends WorkBase {
  readonly type: 'book';

  pages: number;
  isbn: string;
}

export type Work = Article | Whitepaper | Book;

