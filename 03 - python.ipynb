{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Mongo DB tutorial for Python\n",
    "\n",
    "With this quick guide we step through the core features of MongoDB and how to\n",
    "use it.\n",
    "\n",
    "## Setup\n",
    "\n",
    "This workspace already has a [`requirements.txt`](./requirements.txt) which you\n",
    "can use to install all dependencies needed to use MongoDB and [Jupyter\n",
    "Notebook](https://jupyter.org/) - which is the file you are reading right now.\n",
    "\n",
    "I would *highly* recommend using a virtual environment to install the\n",
    "dependencies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [],
   "source": [
    "# Run the venv python package to create a virtual environment\n",
    "# in the .venv folder\n",
    "python -m venv .venv\n",
    "\n",
    "# Activate the virtual environment\n",
    "source ./.venv/bin/activate"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After this, whenever you run `pip install` or `python <somefile>`, you will run\n",
    "against the virtual environment.\n",
    "\n",
    "If you use VSCode, make sure to select the virtual environment as interpreter by\n",
    "opening the command pallet (<kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>P</kbd>) and\n",
    "searching for \"python interpreter\". Select \"Python: Select Interpreter\" and use\n",
    "the python binary in the `.venv` folder. It should be recommended.\n",
    "\n",
    "Next, install the dependencies:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [],
   "source": [
    "pip install -r ./requirements.txt\n",
    "\n",
    "# Or if you just want to use the MongoDB, just run:\n",
    "# pip install pymongo"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Opening a connection\n",
    "\n",
    "Next, we want to connect to a MongoDB. There is a\n",
    "[`docker-compose.yml`](./docker-compose.yml) included in this project. so just\n",
    "run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [],
   "source": [
    "docker-compose up -d # to run as \"deamon\" aka in the background"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "to start a MongoDB instance. The test credentials are \"root\" as user name and\n",
    "\"example\" as password.\n",
    "\n",
    "Now, for the python code, you want to start by importing the `MongoClient` and\n",
    "create an instance of it. To select a specific database, just call\n",
    "`client['db-name']`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Database connected\n"
     ]
    }
   ],
   "source": [
    "from pymongo import MongoClient\n",
    "\n",
    "client = MongoClient('mongodb://root:example@localhost:27017/')\n",
    "\n",
    "db = client['mongo-course']\n",
    "\n",
    "print('Database connected')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This code is also available in [`db.py`](./db.py).\n",
    "\n",
    "Next, we want to select collection(s). This can be done equally easy, by just\n",
    "calling `db['collection-name]`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Collection selected\n"
     ]
    }
   ],
   "source": [
    "books = db['books']\n",
    "\n",
    "print('Collection selected')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interacting with the MongoDB\n",
    "\n",
    "Next we want to insert some data. This is done via Python `dict`s."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Book inserted: 63c12f7d13b14677347bc268\n"
     ]
    }
   ],
   "source": [
    "from datetime import datetime\n",
    "\n",
    "book1 = {\n",
    "  'name': 'Alice in Wonderland',\n",
    "  'release_date': datetime(year=1865,month=11,day=1),\n",
    "  'author': 'Lewis Carroll',\n",
    "}\n",
    "\n",
    "result = books.insert_one(book1)\n",
    "\n",
    "print('Book inserted:', result.inserted_id)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can get all documents in a collection by using `collection.find()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'_id': ObjectId('63c12f7d13b14677347bc268'), 'name': 'Alice in Wonderland', 'release_date': datetime.datetime(1865, 11, 1, 0, 0), 'author': 'Lewis Carroll'}\n"
     ]
    }
   ],
   "source": [
    "# .find without args returns all documents\n",
    "found = books.find()\n",
    "\n",
    "for document in found:\n",
    "  print(document)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice how each document you insert into the DB will have an `_id` set. It uses\n",
    "an `ObjectId`, which is a combination of creation timestamp, host of the DB\n",
    "instance, PID, and a random number. It is guaranteed to be unique in the DB.\n",
    "\n",
    "There is also a default, unique index on the every collection on the `_id`\n",
    "field.\n",
    "\n",
    "You can, however, pass custom values to the `_id` field - it doesn't have to be\n",
    "an `ObjectId`.\n",
    "\n",
    "For frequent or expensive queries, it makes sense to create custom indexes on\n",
    "documents."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'_id_': {'v': 2, 'key': [('_id', 1)]}, 'author_1': {'v': 2, 'key': [('author', 1)]}, 'sequel_1': {'v': 2, 'key': [('sequel', 1)], 'sparse': True}, 'author_1_release_date_1': {'v': 2, 'key': [('author', 1), ('release_date', 1)], 'unique': True}}\n"
     ]
    }
   ],
   "source": [
    "from pymongo import ASCENDING\n",
    "\n",
    "# Simply pass the key to create an index for\n",
    "books.create_index('author')\n",
    "\n",
    "# If not all documents have a specific field, the index must be sparse. Use this\n",
    "# if you don't 100% know the shape of your data!\n",
    "books.create_index('sequel', sparse=True)\n",
    "\n",
    "# Compound indexes span multiple fields at once\n",
    "books.create_index(\n",
    "  # The order does not matter in most cases (only when sorting documents)\n",
    "  [('author', ASCENDING), ('release_date', ASCENDING)],\n",
    "  unique=True # make it unique, so that the combination can only occur once\n",
    ")\n",
    "\n",
    "print(books.index_information())\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Improving DX\n",
    "\n",
    "Inserting `dict`s is not always useful, because they lack type information and\n",
    "therefore easily lead to errors. So we want to create a class for our books."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dataclasses import dataclass\n",
    "from typing import Optional\n",
    "from bson import ObjectId\n",
    "\n",
    "# Use kw_only, so that a constructor is created which only allows passing\n",
    "# arguments as keywords - important for runtime checks\n",
    "@dataclass(kw_only=True)\n",
    "class Book:\n",
    "\n",
    "  _id: Optional[ObjectId] = None\n",
    "\n",
    "  author: str\n",
    "  release_date: datetime\n",
    "  name: str\n",
    "\n",
    "  # Documents can reference other documents, usually via their _id field.\n",
    "  sequel: Optional[ObjectId] = None\n",
    "  "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have a class, we can more easily create documents and save them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'_id': ObjectId('63c12f7d13b14677347bc268'), 'name': 'Alice in Wonderland', 'release_date': datetime.datetime(1865, 11, 1, 0, 0), 'author': 'Lewis Carroll'}\n",
      "{'_id': ObjectId('63c12f8c13b14677347bc269'), 'author': 'Michael Ende', 'release_date': datetime.datetime(1979, 9, 1, 0, 0), 'name': 'The Neverending Story', 'sequel': None}\n",
      "{'_id': ObjectId('63c12f8c13b14677347bc26a'), 'author': 'Johann Wolfgang von Goethes', 'release_date': datetime.datetime(1832, 1, 1, 0, 0), 'name': 'Faust. Der Tragödie zweiter Teil', 'sequel': None}\n",
      "{'_id': ObjectId('63c12f8c13b14677347bc26b'), 'author': 'Johann Wolfgang von Goethes', 'release_date': datetime.datetime(1808, 1, 1, 0, 0), 'name': 'Faust. Eine Tragödie', 'sequel': ObjectId('63c12f8c13b14677347bc26a')}\n"
     ]
    }
   ],
   "source": [
    "book = Book(\n",
    "  author='Michael Ende',\n",
    "  name='The Neverending Story',\n",
    "  release_date=datetime(year=1979, month=9, day=1)\n",
    ")\n",
    "book_dict = book.__dict__\n",
    "del book_dict['_id'] # need to remove _id, otherwise null will be inserted\n",
    "books.insert_one(book_dict)\n",
    "\n",
    "book = Book(\n",
    "  name='Faust. Der Tragödie zweiter Teil',\n",
    "  author='Johann Wolfgang von Goethe',\n",
    "  release_date=datetime(year=1832, month=1, day=1)\n",
    ")\n",
    "book_dict = book.__dict__\n",
    "del book_dict['_id']\n",
    "result = books.insert_one(book_dict)\n",
    "\n",
    "book = Book(\n",
    "  name='Faust. Eine Tragödie',\n",
    "  author='Johann Wolfgang von Goethe',\n",
    "  release_date=datetime(year=1808, month=1, day=1),\n",
    "  sequel=result.inserted_id\n",
    ")\n",
    "book_dict = book.__dict__\n",
    "del book_dict['_id']\n",
    "books.insert_one(book_dict)\n",
    "\n",
    "found = books.find()\n",
    "\n",
    "for document in found:\n",
    "  print(document)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simplifying DB Queries by Abstraction\n",
    "When we define a class, we can abstract complex workflows into methods."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dataclasses import dataclass\n",
    "from typing import Optional\n",
    "from bson import ObjectId\n",
    "from datetime import datetime\n",
    "\n",
    "# Use kw_only, so that a constructor is created which only allows passing\n",
    "# arguments as keywords - important for runtime checks\n",
    "@dataclass(kw_only=True)\n",
    "class WorkBase:\n",
    "\n",
    "  _id: Optional[ObjectId] = None\n",
    "\n",
    "  # Documents can reference other documents, usually via their _id field.\n",
    "  author: list[ObjectId]\n",
    "  references: list[ObjectId]\n",
    "\n",
    "  submittedAt: datetime\n",
    "  field: str\n",
    "\n",
    "  title: str\n",
    "  subtitle: Optional[str]\n",
    "\n",
    "  @staticmethod\n",
    "  def collection() -> Collection:\n",
    "    return db_connector.get_collection(\"works\")\n",
    "\n",
    "  # We abstract the insert procedure for easier use\n",
    "  def insert(self) -> WorkBase():\n",
    "    \"\"\"\n",
    "    Inserts a new work base into the database.\n",
    "    \"\"\"\n",
    "    entity = self.__dict__.copy()  # Copy to avoid side-effects\n",
    "    del entity['_id']\n",
    "    result = self.collection().insert_one(entity)\n",
    "    return self.__class__(**self.collection().find_one({'_id': result.inserted_id}))\n",
    "\n",
    "  # We abstract the update procedure for easier use\n",
    "  def update(self) -> WorkBase:\n",
    "      \"\"\"\n",
    "      Persist the updates done to this work base to the database. The _id of\n",
    "      the object must be set, else a ValueError is raised.\n",
    "      \"\"\"\n",
    "      if self._id is None:\n",
    "        raise ValueError(\"_id must be set when updating Work Bases\")\n",
    "      without_id = self.__dict__.copy()  # Copy to avoid side-effects\n",
    "      del without_id['_id']\n",
    "      with_id = {'_id': self._id}\n",
    "      result = self.collection().find_one_and_update(\n",
    "          filter=with_id,\n",
    "          update={\n",
    "              '$set': without_id,\n",
    "              '$setOnInsert': with_id\n",
    "          },\n",
    "          upsert=True,\n",
    "          return_document=ReturnDocument.AFTER\n",
    "      )\n",
    "      return self.__class__(**result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inheritance\n",
    "As we are using OOP already, we can make use of inheritance.\n",
    "\n",
    "In this example, there is no need to implement the `update` or `insert` methods,\n",
    "as the `Book` class already inherits them from the `WorkBase` class. Note that in the return statements of both `update` and `insert` of the `WorkBase` class, we use `self.__class__` instead of `WorkBase` to make this possible.\n",
    "\n",
    "The `Book` class simply has to define its own additional fields. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dataclasses import dataclass\n",
    "from typing import Optional\n",
    "from bson import ObjectId\n",
    "\n",
    "# Use kw_only, so that a constructor is created which only allows passing\n",
    "# arguments as keywords - important for runtime checks\n",
    "@dataclass(kw_only=True)\n",
    "class Book(WorkBase):\n",
    "\n",
    "    pages: int\n",
    "    isbn: str"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": ".venv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.9"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "cb9b1d6f6755c65ce57fa91c6b270f90395563b08f5d49dbfabdaef67d14b452"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
