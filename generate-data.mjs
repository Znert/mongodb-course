//@ts-check
import { Chance } from "chance";
import { MongoClient, ObjectId } from "mongodb";

/**
 * Import the type definitions.
 *
 * @typedef {import('./data-types.js').User} User
 * @typedef {import('./data-types.js').Work} Work
 * @typedef {import('./data-types.js').WorkBase} WorkBase
 */

const chance = new Chance();

/**
 * Generate a list of users.
 *
 * @param {number} length the number of users to generate.
 * @returns {User[]} the generated users.
 */
function generateUsers(length) {
  console.log(`Genrating ${length} users...`);
  return Array.from({ length }).map(() => {
    const nameParts = chance.name({ suffix: true, prefix: true }).split(" ");
    const [lat, lng] = chance
      .coordinates()
      .split(", ")
      .map((coordinate) => +coordinate);
    /** @type {User} */
    const user = {
      _id: new ObjectId(),
      name: {
        prefix: Math.random() > 0.2 ? nameParts[0] : undefined,
        firstName: nameParts[1],
        lastName: nameParts[2],
        suffix: Math.random() > 0.5 ? nameParts[3] : undefined,
      },
      age: chance.age({ type: "adult" }),
      address:
        Math.random() > 0.3
          ? {
              country: chance.country(),
              state: Math.random() > 0.1 ? chance.state() : undefined,
              city: chance.city(),
              zip: chance.zip(),
              coordinates:
                Math.random() > 0.8
                  ? {
                      type: "Point",
                      coordinates: [lng, lat],
                    }
                  : undefined,
            }
          : undefined,
    };
    return user;
  });
}

/**
 * Generate a list of literary works.
 * 
 * The given list of users is used as potential authors and reviewers of a work.
 * 
 * @param {User[]} users the users to pick from as authors and reviewers.
 * @param {number} length the number of works to generate.
 * @returns {Work[]} the generated works of literature.
 */
function generateWorks(users, length) {
  console.log(
    `Genrating ${length} literary works for ${users.length} users...`
  );
  // First generate the IDs
  const workIds = Array.from({ length }).map(() => new ObjectId());

  const fields = [
    "biology",
    "chemistry",
    "computer science",
    "physics",
    "architecture",
    "robotics",
  ];
  const journals = [
    "The Journal",
    "Science All The Way",
    "Centrifuge",
    "Humbug",
    "Serious Labs",
  ];
  const journalEditions = Array.from({ length: 100 }).flatMap((_, yearOffset) => [
    `H1 ${1920 + yearOffset}`,
    `H2 ${1920 + yearOffset}`,
  ]);

  /**
   * Generate the base for a literary work.
   *
   * @param {number} index the index of the work to generate
   * @returns {WorkBase} the generated work base.
   */
  function generateBase(index) {
    // Pick between 1 and 10 authors, with higher probability for less authors.
    const numberAuthors = 1 + Math.round(Math.random() ** 8 * 9);
    // Same for references, but between 10 and 100.
    const numberReferences = 10 + Math.round(Math.random() ** 5 * 90);
    return {
      _id: workIds[index],
      authors: chance.pickset(users, numberAuthors).map((user) => user._id),
      references: chance.pickset(workIds, numberReferences),
      submittedAt: /** @type Date */ (
        chance.date({ year: chance.integer({ min: 1910, max: 2022 }) })
      ),
      field: chance.pickone(fields),

      title: chance.sentence({ words: chance.integer({ min: 3, max: 10 }) }),
      subtitle:
        Math.random() > 0.15
          ? chance.sentence({ words: chance.integer({ min: 5, max: 20 }) })
          : undefined,
    };
  }

  // Create a generator for each work type
  /** @type { {[WorkType in Work['type']]: (index: number) => Work & {type: WorkType}} } */
  const generators = {
    article: (index) => ({
      ...generateBase(index),
      type: "article",

      journal: chance.pickone(journals),
      edition: chance.pickone(journalEditions),
      reviewedBy: chance
        .pickset(users, 1 + Math.round(Math.random() ** 5 * 4))
        .map((user) => user._id),
    }),
    "white-paper": (index) => ({
      ...generateBase(index),
      type: "white-paper",

      publication: Math.random() > 0.7 ? chance.pickone(workIds) : undefined,
    }),

    book: (index) => ({
      ...generateBase(index),
      type: "book",

      pages: chance.integer({ min: 50, max: 500 }),
      isbn: `ISBN 978-${chance.integer({ min: 0, max: 9 })}-${chance.integer({
        min: 1000,
        max: 9999,
      })}-${chance.integer({ min: 10, max: 999 })}-${chance.integer({
        min: 1,
        max: 9,
      })}`,
    }),
  };
  /** @type Work['type'][] */
  const types = [
    "article",
    "article",
    "article",
    "white-paper",
    "white-paper",
    "book",
  ];

  return Array.from({ length }).map((_, index) => {
    return generators[chance.pickone(types)](index);
  });
}

// Fill database
const mongoClient = new MongoClient("mongodb://root:example@127.0.0.1:27017/");
const db = mongoClient.db("mongo-course");

/** @type {import('mongodb').Collection<User>} */
const users = db.collection("users");
/** @type {import('mongodb').Collection<Work>} */
const works = db.collection("works");

async function fillDatabase() {
  // First, remove all data
  await db.dropDatabase();

  // (Re-)initialize the indexes
  await Promise.all([
    users.createIndex({ "name.prefix": 1 }, { sparse: true }),
    users.createIndex({ "name.firstName": 1 }, { sparse: true }),
    users.createIndex({ "name.lastName": 1 }, { sparse: true }),
    users.createIndex({ "name.suffix": 1 }, { sparse: true }),
    users.createIndex({ age: 1 }, { sparse: true }),
    users.createIndex({ "address.country": 1 }, { sparse: true }),
    users.createIndex({ "address.state": 1 }, { sparse: true }),
    users.createIndex({ "address.city": 1 }, { sparse: true }),
    users.createIndex({ "address.zip": 1 }, { sparse: true }),
    users.createIndex({ "address.coordinates": "2dsphere" }, { sparse: true }),

    works.createIndex({ type: 1 }),
    works.createIndex({ title: 1 }, { sparse: true }),
    works.createIndex({ subtitle: 1 }, { sparse: true }),

    // Index for articles
    works.createIndex({ journal: 1 }, { sparse: true }),
    works.createIndex({ edition: 1 }, { sparse: true }),
    works.createIndex({ reviewedBy: 1 }, { sparse: true }),

    // Index for papers
    works.createIndex({ publication: 1 }, { sparse: true }),

    // Index for books
    works.createIndex({ isbn: 1 }, { sparse: true }),
  ]);

  // Insert mock data
  const userData = generateUsers(1_000);
  await users.insertMany(userData);

  const workData = generateWorks(userData, 10_000);
  await works.insertMany(workData);

  // Finally, close the MongoDB connection
  await mongoClient.close();
}

fillDatabase();
