#!/bin/bash

mongorestore --db mongo-course --username root --password example --authenticationDatabase=admin --archive="/dumps/mongo-course-dump"