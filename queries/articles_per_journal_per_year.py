from pymongo import MongoClient

# Requires the PyMongo package.
# https://api.mongodb.com/python/current

client = MongoClient('mongodb://root:example@localhost:27017/?authMechanism=DEFAULT')
result = client['mongo-course']['works'].aggregate([
    {
        '$match': {
            'type': 'article', 
            'journal': {
                '$ne': None
            }
        }
    }, {
        '$group': {
            '_id': {
                'journal': '$journal', 
                'edition': '$edition'
            }, 
            'articlesPerEdition': {
                '$push': '$$ROOT'
            }
        }
    }, {
        '$group': {
            '_id': '$_id.journal', 
            'articlesPerEdition': {
                '$push': {
                    'edition': '$_id.edition', 
                    'articles': '$articlesPerEdition'
                }
            }
        }
    }, {
        '$addFields': {
            'articlesPerEdition': {
                '$sortArray': {
                    'input': '$articlesPerEdition', 
                    'sortBy': {
                        'edition': 1
                    }
                }
            }
        }
    }, {
        '$addFields': {
            'articlesPerEdition': {
                '$arrayToObject': {
                    '$map': {
                        'input': '$articlesPerEdition', 
                        'as': 'perEdition', 
                        'in': {
                            'k': '$$perEdition.edition', 
                            'v': '$$perEdition.articles'
                        }
                    }
                }
            }
        }
    }
])