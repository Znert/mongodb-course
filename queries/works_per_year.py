from pymongo import MongoClient

# Requires the PyMongo package.
# https://api.mongodb.com/python/current

client = MongoClient('mongodb://root:example@localhost:27017/?authMechanism=DEFAULT')
result = client['mongo-course']['works'].aggregate([
    {
        '$group': {
            '_id': {
                '$year': '$submittedAt'
            }, 
            'works': {
                '$push': '$$ROOT'
            }
        }
    }, {
        '$sort': {
            '_id': 1
        }
    }, {
        '$addFields': {
            'totalWorks': {
                '$size': '$works'
            }, 
            'totalArticles': {
                '$size': {
                    '$filter': {
                        'input': '$works', 
                        'as': 'work', 
                        'cond': {
                            '$eq': [
                                '$$work.type', 'article'
                            ]
                        }
                    }
                }
            }, 
            'totalJournals': {
                '$size': {
                    '$filter': {
                        'input': '$works', 
                        'as': 'work', 
                        'cond': {
                            '$eq': [
                                '$$work.type', 'journal'
                            ]
                        }
                    }
                }
            }
        }
    }
])