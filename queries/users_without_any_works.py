from pymongo import MongoClient

# Requires the PyMongo package.
# https://api.mongodb.com/python/current

client = MongoClient('mongodb://root:example@localhost:27017/?authMechanism=DEFAULT')
result = client['mongo-course']['users'].aggregate([
    {
        '$lookup': {
            'from': 'works', 
            'localField': '_id', 
            'foreignField': '_authors', 
            'as': 'works'
        }
    }, {
        '$match': {
            'works': {
                '$size': 0
            }
        }
    }, {
        '$unset': 'works'
    }
])