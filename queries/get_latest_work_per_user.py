from pymongo import MongoClient

# Requires the PyMongo package.
# https://api.mongodb.com/python/current

client = MongoClient(
    'mongodb://root:example@localhost:27017/?authMechanism=DEFAULT')
result = client['mongo-course']['users'].aggregate([
    {
        '$lookup': {
            'from': 'works',
            'localField': '_id',
            'foreignField': 'authors',
            'as': 'works'
        }
    }, {
        '$addFields': {
            'latestWorks': {
                '$filter': {
                    'input': '$works',
                    'as': 'work',
                    'cond': {
                        '$eq': [
                            '$$work.submittedAt', {
                                '$max': '$works.submittedAt'
                            }
                        ]
                    }
                }
            }
        }
    }, {
        '$unset': 'works'
    }, {
        '$addFields': {
            'latestWorksSize': {
                '$size': '$latestWorks'
            }
        }
    }
])
