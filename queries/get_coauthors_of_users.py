from pymongo import MongoClient

# Requires the PyMongo package.
# https://api.mongodb.com/python/current

client = MongoClient('mongodb://root:example@localhost:27017/?authMechanism=DEFAULT')
result = client['mongo-course']['users'].aggregate([
    {
        '$lookup': {
            'from': 'works', 
            'localField': '_id', 
            'foreignField': 'authors', 
            'as': 'coauthors'
        }
    }, {
        '$addFields': {
            'coauthors': '$coauthors.authors'
        }
    }, {
        '$addFields': {
            'coauthors': {
                '$reduce': {
                    'input': '$coauthors', 
                    'initialValue': [], 
                    'in': {
                        '$setUnion': [
                            '$$value', '$$this'
                        ]
                    }
                }
            }
        }
    }, {
        '$addFields': {
            'lengthBefore': {
                '$size': '$coauthors'
            }
        }
    }, {
        '$addFields': {
            'coauthors': {
                '$filter': {
                    'input': '$coauthors', 
                    'as': 'coauthor', 
                    'cond': {
                        '$ne': [
                            '$$coauthor', '$_id'
                        ]
                    }
                }
            }
        }
    }, {
        '$addFields': {
            'lengthAfter': {
                '$size': '$coauthors'
            }
        }
    }
])