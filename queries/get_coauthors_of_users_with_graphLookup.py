from pymongo import MongoClient

# Requires the PyMongo package.
# https://api.mongodb.com/python/current

client = MongoClient('mongodb://root:example@localhost:27017/?authMechanism=DEFAULT')
result = client['mongo-course']['users'].aggregate([
    {
        '$graphLookup': {
            'from': 'works', 
            'startWith': '$_id', 
            'connectFromField': '_id', 
            'connectToField': 'authors', 
            'as': 'coauthors', 
            'maxDepth': 1
        }
    }
])