from pymongo import MongoClient

# Requires the PyMongo package.
# https://api.mongodb.com/python/current

client = MongoClient('mongodb://root:example@localhost:27017/?authMechanism=DEFAULT')
result = client['mongo-course']['works'].aggregate([
    {
        '$group': {
            '_id': 'title-stats', 
            'avgTitleLength': {
                '$avg': {
                    '$strLenCP': '$title'
                }
            }, 
            'maxTitleLength': {
                '$max': {
                    '$strLenCP': '$title'
                }
            }, 
            'minTitleLength': {
                '$min': {
                    '$strLenCP': '$title'
                }
            }
        }
    }
])